Typescript RPC stubs generator from [swagger.io](http://swagger.io/)-like JSON description to use with [expressjs](http://expressjs.com/)

# solar-swagger

[![NPM](https://nodei.co/npm/solar-swagger.png?downloads=true&downloadRank=true)](https://nodei.co/npm/solar-swagger/)

[![npm version](https://badge.fury.io/solar-swagger.svg)](http://badge.fury.io/solar-swagger)
[![build status](https://travis-ci.org/solarlizard/solar-swagger.svg?branch=master)](https://travis-ci.org/solarlizard/solar-swagger.svg?branch=master)
[![Dependencies](https://david-dm.org/solarlizard/solar-swagger.svg)](https://david-dm.org/solarlizard/solar-swagger)
[![devDependency Status](https://david-dm.org/solarlizard/solar-swagger/dev-status.svg)](https://david-dm.org/solarlizard/solar-swagger#info=devDependencies)

# Usage

**Note:** This project is just less then alpha-version - it's just experiments. Wait for release
