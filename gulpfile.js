var gulp = require("gulp");
var del = require('del');
var ts = require("gulp-typescript");
var mocha = require('gulp-mocha');
var merge = require('merge2');

var tsCompilerOptions = {
    experimentalDecorators: true,
    noImplicitAny: true,
    suppressImplicitAnyIndexErrors: true,
    target: "es6",
    module: "commonjs"
};

gulp.task('clean', function (cb) {
    del.sync(['./src/generated']);
    del.sync(['./target']);
    cb();
});

gulp.task('main-compile', function () {
    return result = gulp.src('./src/main/index.ts')
        .pipe(ts(tsCompilerOptions))
        .pipe(gulp.dest('./target/build/main'));
});

gulp.task('test-generate-server', ['main-compile'], function () {
    return gulp
            .src("./src/test/test.json")
            .pipe(require('./target/build/main/index').generate({
                mode : "server",
                referencePath : "../../typings/index.d.ts"
            }))
            .pipe(gulp.dest('./src/generated'));
});

gulp.task('test-generate-client', ['main-compile'], function () {
    return gulp
            .src("./src/test/test.json")
            .pipe(require('./target/build/main/index').generate({
                mode : "client"
            }))
            .pipe(gulp.dest('./src/generated'));
});

gulp.task('test-compile-generated', ['test-generate-server','test-generate-client'], function () {
    return gulp.src('./src/generated/**/*.ts')
            .pipe(ts(tsCompilerOptions))
            .pipe(gulp.dest('./target/build/generated'));
});

gulp.task('test-compile', ['test-compile-generated'], function () {
    return gulp.src('./src/test/**/*.ts')
            .pipe(ts(tsCompilerOptions))
            .pipe(gulp.dest('./target/build/test'));
});

gulp.task('test', ['test-compile'], function () {
    return gulp
            .src('./target/build/test/*Test.js')
            .pipe(mocha({}))
            .once('error', function () {
                process.exit(1);
            });
});

gulp.task('default', ['test'], function () {
});
