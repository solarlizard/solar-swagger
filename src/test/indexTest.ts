/// <reference path="../../typings/index.d.ts" />
/// <reference path="../../typings/globals/mocha/index.d.ts" />
import * as express from "express";
import * as bodyParser from "body-parser";
import * as request from "supertest";
import * as http from "http";
import chai = require('chai');

import * as server from "./../generated/test.json.server";
import * as client from "./../generated/test.json.client";


var expect = chai.expect;

class Controller implements server.Service {

    public async getNoParams(req?: express.Request, res?: express.Response): Promise<void> {
        res.statusCode = 202;
    }

    public async getSingleNumber(parameter: number, req?: express.Request, res?: express.Response): Promise<number> {
        expect(parameter).equals (10);
        
        return 20;
    }

    public async getArrayNumber(parameter: number[], req?: express.Request, res?: express.Response): Promise<number[]> {
        
        expect(parameter[0]).equals (10);
        expect(parameter[1]).equals (20);
        
        return [30,40];
    }

    public async getSingleString(parameter: string, req?: express.Request, res?: express.Response): Promise<string> {
        expect(parameter).equals ("req");
        
        return "res";
    }

    public async getArrayString(parameter: string[], req?: express.Request, res?: express.Response): Promise<string[]> {
        
        expect(parameter[0]).equals ("req1");
        expect(parameter[1]).equals ("req2");
        
        return ["res1","res2"];
    }

    public async getSingleDate(parameter: Date, req?: express.Request, res?: express.Response): Promise<Date> {
        res.statusCode = 202;
        return null;
    }

    public async getArrayDate(parameter: Date[], req?: express.Request, res?: express.Response): Promise<Date[]> {
        res.statusCode = 202;
        return null;
    }

    public async getSingleEnum(parameter: server.COLOR, req?: express.Request, res?: express.Response): Promise<server.COLOR> {
        res.statusCode = 202;
        return null;
    }

    public async getArrayEnum(parameter: server.COLOR[], req?: express.Request, res?: express.Response): Promise<server.COLOR[]> {
        res.statusCode = 202;
        return null;
    }

    public async getSingleModel(parameter: server.ChildModel, req?: express.Request, res?: express.Response): Promise<server.ChildModel> {
        res.statusCode = 202;
        return null;
    }

    public async getArrayModel(parameter: server.ChildModel[], req?: express.Request, res?: express.Response): Promise<server.ChildModel[]> {
        res.statusCode = 202;
        return null;
    }
}

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

server.register(app, new Controller(), server.defaultErrorCallback);

var doneCallback = (err: any, done: MochaDone) => {
    if (err) {
        done(err);
    }
    else {
        done();
    }
}

it('getNoParams', (done: MochaDone) => {request(app).put("/getNoParams").expect(200).end((err) => { doneCallback(err, done)});});
it('getSingleNumber', (done: MochaDone) => { request(app).put("/getSingleNumber").send({ parameter: 10 }).expect("20").end((err) => { doneCallback(err, done)});});
it('getArrayNumber', (done: MochaDone) => { request(app).put("/getArrayNumber").send({ parameter: [10,20] }).expect("[30,40]").end((err) => { doneCallback(err, done)});});
//it('getSingleString', (done: MochaDone) => { request(app).put("/getSingleString").send({ parameter: "req" }).expect("res").end((err) => { doneCallback(err, done)});});
//it('getArrayString', (done: MochaDone) => { request(app).put("/getArrayString").send({ parameter: ["req1","req2"] }).expect(["res1","res2"]).end((err) => { doneCallback(err, done)});});
//it('getArrayString', (done: MochaDone) => {request(app).put("/getNoParams").expect(202).end((err) => { doneCallback(err, done)});});
//it('getSingleDate', (done: MochaDone) => {request(app).put("/getNoParams").expect(202).end((err) => { doneCallback(err, done)});});
//it('getArrayDate', (done: MochaDone) => {request(app).put("/getNoParams").expect(202).end((err) => { doneCallback(err, done)});});
//it('getSingleEnum', (done: MochaDone) => {request(app).put("/getNoParams").expect(202).end((err) => { doneCallback(err, done)});});
//it('getArrayEnum', (done: MochaDone) => {request(app).put("/getNoParams").expect(202).end((err) => { doneCallback(err, done)});});
//it('getSingleModel', (done: MochaDone) => {request(app).put("/getNoParams").expect(202).end((err) => { doneCallback(err, done)});});
//it('getArrayModel', (done: MochaDone) => {request(app).put("/getNoParams").expect(202).end((err) => { doneCallback(err, done)});});
