/// <reference path="../../typings/index.d.ts" />
import * as through2 from "through2";
import * as gulp from "gulp";

export enum MODE {
    server = <any>"server",
    client = <any>"client"
}

export interface Options {
    mode : MODE
    referencePath : string
}

export function generate(options: Options) {
    return through2.obj(function(file: any, enc: string, cb: Function) {
        try {

            var builder = new Builder ();
            
            builder.build(file.contents.toString(), options);

            if (options.mode == MODE.client) {
                file.contents = new Buffer(builder.clientBuffer.toString(), "utf-8");
                file.path = file.path = file.path + ".client.ts";;
            }
            else if (options.mode == MODE.server) {
                file.contents = new Buffer(builder.serverBuffer.toString(), "utf-8");
                file.path = file.path = file.path + ".server.ts";;
            }
            
            cb(null, file);
        }
        catch (err) {
            cb(err);
        }
    });
}

class Builder {
    public serverBuffer: string = "";
    public clientBuffer: string = "";
    
    private api : Api;
    
    public build(json: string, options: Options) {
        this.api = JSON.parse(json);
        
        this.emitServerStatic(options);
        this.emitClientStatic();
        
        this.emitDefinitions();
        
        this.emitServer(`\r\nexport interface Service {\r\n\r\n`);
        
        this.emitPaths();
        
        this.emitServer(`}`);
        
        this.emitServerRegistration();
    }
    
    private emitClientStatic () {
        this.emitClient(`
export var ROOT_URL: string = "/";

export function getUrl (url : string) : string {
    return (ROOT_URL + url).replace ("//","/");
}

export interface Callback<MODEL> {
    success? : (model? : MODEL) => void;
    exception? : (exception : SwaggerException)=> void;
    completed? : ()=> void
}

export class CallbackAdapter<MODEL> implements Callback<MODEL> {
    public success : (model ? : MODEL) => void;
    public exception: (exception?: SwaggerException) => void;
    public completed: () => void;
    
    constructor (success? : (model ? : MODEL) => void, exception?: (exception?: SwaggerException) => void, completed?: () => void) {
        this.success = success;
        this.exception = exception;
        this.completed = completed;
    }
}

export function ajax<MODEL>(method : string, url: string, param : any, callback: Callback<MODEL>) {

    var ajaxRequest = {
        type: method,
        headers: {"Accept": "application/json","Content-Type": "application/json"},
        contentType: "application/json",
        url: url,
        timeout: 30000,
        success: (responseModel : any) => {
            try {
                if (callback.success) {
                    callback.success(responseModel);
                }
            }
            finally {
                if (callback.completed) {
                    callback.completed();
                }
            }
        },
        error: (error : any) => {
            try {
                if (error.status == 200) {
                    if (callback.success) {
                        callback.success(error.responseText);
                    }
                }
                else {
                    var swaggerException: SwaggerException = JSON.parse(error.responseText);
                    
                    if (callback.exception){
                        if (swaggerException.className != null) {
                            callback.exception(swaggerException);
                        }
                        else {
                            callback.exception(new SwaggerException ());
                        }
                    }
                    else {
                        console.log(error.responseText);
                        window.alert("Internal error!");
                    }
                }
            }
            finally {
                if (callback.completed) {
                    callback.completed();
                }
            }
        }
    };

    if (param != null) {

        jQuery.extend(ajaxRequest, {
            data : JSON.stringify({parameter : param}),
            dataType: "json"
        });
    }

    $.ajax(ajaxRequest);
} 
`);
    }
    
    private emitServerStatic(options: Options) {
        this.emitServer(`
/// <reference path="${options.referencePath}" />
import * as express from "express";

export function defaultErrorCallback(err: any, req: express.Request, res: express.Response) {
    if (err != null) {
        var swaggerException: SwaggerException = err;

        if (swaggerException.className == null) {
            console.log (err);
            res.status(500).end();
        }
        else {
            res.status(400).send(JSON.stringify(swaggerException)).end ();
        }
        return false;
    }
    else {
        return true;
    }
}

`);
        
    }
    
    private emitDefinitions () {
        var definitionName: string;

        for (definitionName in this.api.definitions) {
            this.emitDefinition(definitionName, this.api.definitions[definitionName]);
        }
    }
    
    private emitDefinition (definitionName : string, definition : Definition) {
        var definition = this.api.definitions[definitionName];

        if (definition.type == "string") {
            var definition = this.api.definitions[definitionName];
            
            this.emitEnumDefinition(definitionName, definition);
        }
        else if (definition.type == "object") {
            this.emitModelDefinition(definitionName, definition);
        }
    }
    
    private emitEnumDefinition (definitionName : string, definition : Definition) {
        this.emit(`\r\nexport enum ${definitionName} {\r\n`);
        
        for (var q = 0; q < definition.enum.length; q++) {
            var enumValue = definition.enum[q];
            
            this.emit (`    ${enumValue}=<any>"${enumValue}"`);
            
            if (q < definition.enum.length - 1) {
                this.emit (",\r\n");
            }
        }

        this.emit(`\r\n}\r\n`);
    }
    
    private emitModelDefinition (definitionName : string, definition : Definition) {
        this.emit(`\r\nexport class ${definitionName}`);

        if (definition.allOf != null) {
            this.emit(` extends ${this.getNameFromRef(<Ref>definition.allOf[0])}`);
        }

        this.emit(` {\r\n`);
        
        if (definitionName.indexOf("Exception") == definitionName.length - "Exception".length) {
            this.emit(`    static CLASS_NAME : string = "${definitionName}";\r\n\r\n`);
            
            if (definitionName == "SwaggerException") {
                this.emit(`    public className : string = "${definitionName}";\r\n`);
            }
            else {
                this.emit(`    constructor () {\r\n`);
                this.emit(`        super ();\r\n`);
                this.emit(`        this.className = "${definitionName}";\r\n`);
                this.emit(`    }\r\n`);
            }
        }
        
        this.emitModelProperties(definition.properties);
        
        if (definition.allOf != null) {
            if (definition.allOf.length == 2) {
                this.emitModelProperties(<{ [key: string]: Property }>definition.allOf[1].properties);
            }
        }
        
        this.emit(`\r\n}\r\n`);
    }
    
    private emitModelProperties (propertyMap : { [key: string]: Property }) {
        var propertyName : string;
        
        for (propertyName in propertyMap) {
            if (propertyName != "className") {
                this.emitModelProperty(propertyName, propertyMap[propertyName]);
            }
        }
    }
    
    private emitModelProperty(propertyName: string, property: Property) {
        this.emit(`\r\n    public ${propertyName} : ${this.getTypeLiteralFromSchema(property)};`);
    }
    
    private emitPaths () {
        var path : string;
        
        for (path in this.api.paths) {
            var putMethod = this.api.paths[path]["put"];

            if (putMethod != null) {
                this.emitServerMethod(path, putMethod);
                this.emitClientMethod(path, putMethod);
            }
        }
    }

    private emitServerMethod(path: string, method: Method) {
        this.emitServer(`    ${this.getOperationNameFromPath(path)} (${this.getMethodParameterList(method)}req? : express.Request, res? : express.Response) : Promise<${this.getMethodResponseType(method)}>\r\n\r\n`);
    }
    
    private emitClientMethod(path: string, method: Method) {
        this.emitClient(`\r\nexport function ${this.getOperationNameFromPath(path)} (${this.getMethodParameterList(method)}callback : Callback<${this.getMethodResponseType(method)}>){\r\n`);
        this.emitClient(`    ajax("PUT", getUrl ("${path}")`);
        
        if (method.parameters != null && method.parameters.length == 1) {
            this.emitClient(`, ${method.parameters[0].name}`);
        }
        else {
            this.emitClient(`, null`);
        }
        
        this.emitClient(`, callback);\r\n`);
        this.emitClient(`}\r\n`);
    }
    
    private emitServerRegistration () {
        this.emitServer(`\r\nexport function register(app: express.Express, service: Service, errorCallback : (err : any, req : express.Request, res : express.Response)=> void) {`);
        
        var path : string;
        
        for (path in this.api.paths) {
            var putMethod = this.api.paths[path]["put"];

            if (putMethod != null) {
                this.emitServerRegistrationPath(path, putMethod);
            }
        }
        
        this.emitServer(`}`);
    }
    
    private emitServerRegistrationPath (path : string, method : Method ) {
        
        this.emitServer(`\r\n    app.put("${path}", (req : express.Request, res : express.Response) => {`);
        this.emitServer(`\r\n        service.${this.getOperationNameFromPath(path)} (`);
        if (method.parameters != null && method.parameters.length == 1) {
            this.emitServer(`req.body.parameter, `);
        }
        this.emitServer(`req, res)`);
        this.emitServer(`\r\n        .then ((result? : any) => {`);
        this.emitServer(`\r\n            res.status(200).send(JSON.stringify(result)).end;`);
        this.emitServer(`\r\n        })`);
        this.emitServer(`\r\n        .catch ((err : any) => {`);
        this.emitServer(`\r\n            errorCallback (err, req, res);`);
        this.emitServer(`\r\n        })`);
        this.emitServer(`\r\n    });\r\n`);
    }

    private getMethodParameterList (method: Method) {
        
        if (method.parameters != null && method.parameters.length == 1) {
            var parameter = method.parameters[0];

            return `${parameter.name} : ${this.getTypeLiteralFromSchema(parameter.schema)}, `;
            
        }
        else {
            return "";
        }
    }
    
    private getMethodResponseType (method: Method) : string {
        if (method.responses[200].schema == null) {
            return `void`;
        }
        else {
            return this.getTypeLiteralFromSchema(method.responses[200].schema);
        }
    }
    
    private getTypeLiteralFromSchema(schema: Schema) : string {
        if (schema.$ref != null) {
            return this.getNameFromRef(schema);
        }
        else if (schema.type == "array") {
            return this.getTypeLiteralFromRef(schema.items) + " []";
        }
        else {
            return schema.type;
        }
    }

    private getTypeLiteralFromRef (ref: Ref) : string {
        if (ref.$ref != null) {
            return this.getNameFromRef(ref);
        }
        else {
            return ref.type;
        }
    }

    private getNameFromRef(ref: Ref): string {
        return ref.$ref.substring(ref.$ref.lastIndexOf("/") + 1);
    }

    private getOperationNameFromPath(path: string): string {
        return path.substring(path.lastIndexOf("/") + 1);
    }
    
    private emit(data : string) {
        this.emitServer(data);
        this.emitClient(data);
    }
    
    private emitServer(data : string) {
        this.serverBuffer += data;
    }

    private emitClient(data : string) {
        this.clientBuffer += data;
    }
}

interface Api {
    paths: { [key: string]: { [key: string]: Method } };
    definitions: { [key: string]: Definition };
}

interface Method {
    parameters: Parameter[];
    responses: Response[];
}

interface Parameter {
    in: string;
    name: string;
    schema: Schema;
}

interface Response {
    in: string;
    name: string;
    schema: Schema;
}

interface Schema extends Ref {
    items: Ref;
}

interface Definition {
    type: string;
    enum: string[];
    properties: { [key: string]: Property };
    allOf: any[];
}

interface Property extends Ref {
    items: Ref;
}

interface Ref {
    $ref: string;
    type: string;
}
